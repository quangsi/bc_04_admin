import { Space, Table, Tag } from "antd";
import React from "react";
import { headColumns } from "./utitls.usermanagement";

const UserTable = ({ userList }) => (
  <Table columns={headColumns} dataSource={userList} />
);

export default UserTable;
