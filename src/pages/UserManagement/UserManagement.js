import React, { useEffect, useState } from "react";
import { userServ } from "../../services/userService";
import UserTable from "./UserTable";

export default function UserManagement() {
  const [userList, setUserList] = useState([]);
  useEffect(() => {
    userServ
      .getUserList()
      .then((res) => {
        setUserList(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container mx-auto">
      <UserTable userList={userList} />
    </div>
  );
}
