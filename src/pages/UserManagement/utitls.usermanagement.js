export const headColumns = [
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Tên khách hàng",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Loại tài khoản",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
  },
];
// email: "hotanphat@gmail.com";
// hoTen: "Ho Tan Phat 12";
// maLoaiNguoiDung: "QuanTri";
// matKhau: "Admin124";
// soDT: "0789476036";
// taiKhoan: "123456";
